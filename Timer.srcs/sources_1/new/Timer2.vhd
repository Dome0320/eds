----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.02.2024 20:45:42
-- Design Name: 
-- Module Name: Timer2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Timer2 is
  Port ( sys_clk : in std_logic;
         state: in std_logic;
         interrupt: out std_logic;
         value_out: out std_logic_vector(7 downto 0);  
         value_in: in std_logic_vector(7 downto 0);
         prescaler: in std_logic_vector(2 downto 0)
        );
end Timer2;

architecture Behavioral of Timer2 is
signal prevalue: integer range 0 to 128 := 128;
signal real_clk: std_logic := '0';
shared variable temp_number: integer range 0 to 255;
signal prescaler_value: integer range 0 to 127;
begin
--.g
process(prescaler)
begin
case prescaler is
    when "111" => prescaler_value <= 127;
    when "110" => prescaler_value <= 63;
    when "101" => prescaler_value <= 31;
    when "100" => prescaler_value <= 15;
    when "011" => prescaler_value <= 7;
    when "010" => prescaler_value <= 3;
    when "001" => prescaler_value <= 1;
    when "000" => prescaler_value <= 0;
end case;
end process;
process(sys_clk,prevalue,prescaler,prescaler_value,real_clk,state) --State Maschine f�r Stopp, run und �ndern
begin
if (sys_clk'event and rising_edge(sys_clk)) then
           if prevalue = prescaler_value then
            prevalue <= 0;
            if real_clk = '0' then
                real_clk <= '1';
            else 
                real_clk <= '0';
            end if;
           else
            prevalue <= prevalue + 1;
           end if;
end if;
end process;
process(real_clk,value_in)
begin
if (real_clk'event and rising_edge(real_clk)) then -- triggere auf beiden Flanken or falling_edge(real_clk)
    if temp_number /= 255 then 
        temp_number := temp_number + 1;
        value_out <= std_logic_vector(to_unsigned(temp_number, 8));
    else
        interrupt <= '1';
        temp_number := to_integer(unsigned(value_in));
        value_out <= value_in;
    end if;
end if;
end process;
--process zum Anzeigen -> kann ich da einfach die Entsprechenden pins belegen??
--Ich habe jetzt state in den prozess f�r sys clock eingebaut aber vielleicht muss das mehr wie andere Projekte aussehen
end Behavioral;
